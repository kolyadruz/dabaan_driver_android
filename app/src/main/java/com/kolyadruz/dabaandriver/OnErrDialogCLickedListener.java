package com.kolyadruz.dabaandriver;

public interface OnErrDialogCLickedListener {

    void onOkButtonCLicked();

    void onCancelButtonClicked();

    void onRetryButtonClicked();

}
