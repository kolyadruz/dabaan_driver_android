package com.kolyadruz.dabaandriver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public final class Global {

    public static final String api_url = "http://dabaan.rvcode.ru/mobile/";

    public static final String SOCKET_SERVER_URL = "http://dabaan.rvcode.ru:3101";

    public static void showErrId(final Activity activity, final Integer err_id, final OnErrDialogCLickedListener listener) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (activity != null && !activity.isFinishing()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                    String message = "";

                    if (err_id == 1) {
                        message = "Указан неправильный номер телефона";
                    } else if (err_id == 2) {
                        message = "Повторите попытку позднее";
                    } else if (err_id == 3) {
                        message = "Ошибка работы токена";
                    } else if (err_id == 4) {
                        message = "Заполните все поля";
                    } else if (err_id == 5) {
                        message = "Ваш аккаунт неактивен";
                    } else if (err_id == 6) {
                        message = "Пользователь отсутствует или заблокирован";
                    } else if (err_id == 7) {
                        message = "Заказ закрыт, или отменен";
                    } else if (err_id == 8) {
                        message = "Недостаточно средств на балансе";
                    } else if (err_id == 9) {
                        message = "Заказ уже принят";
                    } else if (err_id == 10) {
                        message = "Ошибка интернет соединения";
                    }

                    builder.setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    listener.onOkButtonCLicked();
                                }
                            });
                    AlertDialog alert = builder.create();

                    alert.show();
                }
            }
        });
    }

    public static void showErrorConnection (final Activity activity, final OnErrDialogCLickedListener listener) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (activity != null && !activity.isFinishing()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Остуствует интернет подключение")
                            .setCancelable(false)
                            .setPositiveButton("ПОВТОРИТЬ", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    listener.onRetryButtonClicked();
                                }
                            });
                    AlertDialog alert = builder.create();

                    alert.show();
                }
            }
        });

    }

}
