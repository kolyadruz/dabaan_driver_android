package com.kolyadruz.dabaandriver;

/**
 * Created by kolyadruz on 28.03.2018.
 */

import android.content.ContentResolver;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.kolyadruz.dabaandriver.networking.NetworkService;
import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.DoneOrderData;
import com.kolyadruz.dabaandriver.Views.auth.AutoLogin;
import com.kolyadruz.dabaandriver.utils.NotificationUtils;
import com.kolyadruz.dabaandriver.vo.NotificationVO;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgingService";
    private static final String TITLE = "title";
    private static final String EMPTY = "";
    private static final String MESSAGE = "message";
    private static final String IMAGE = "image";
    private static final String ACTION = "action";
    private static final String DATA = "data";
    private static final String ACTION_DESTINATION = "action_destination";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            handleData(data);

        } else if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification());
        }// Check if message contains a notification payload.

    }
    // [END receive_message]

    private void handleNotification(RemoteMessage.Notification RemoteMsgNotification) {
        String message = RemoteMsgNotification.getBody();
        String title = RemoteMsgNotification.getTitle();
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);

        Intent resultIntent = new Intent(getApplicationContext(), AutoLogin.class);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());

        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();

    }

    private void handleData(Map<String, String> data) {
        String title = data.get(TITLE);
        String message = data.get(MESSAGE);
        String iconUrl = data.get(IMAGE);
        String action = data.get(ACTION);
        String actionDestination = data.get(ACTION_DESTINATION);
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);

        String iconDabaan = ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + getApplicationContext().getPackageName() + "/drawable/ic_notification";

        notificationVO.setIconUrl(iconDabaan);
        notificationVO.setAction(action);
        notificationVO.setActionDestination(actionDestination);

        Intent resultIntent = new Intent(getApplicationContext(), AutoLogin.class);

        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());


        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();

    }

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        FirebaseMessaging.getInstance().subscribeToTopic("drivers");

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String fbtoken) {
        // TODO: Implement this method to send token to your app server.

        RequestBody body = new RequestBody();
        body.fb_token = fbtoken;

        NetworkService.getInstance()
                .getJSONApi()
                .updatetoken(body)
                .enqueue(new Callback<DoneOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<DoneOrderData> call, @NonNull Response<DoneOrderData> response) {

                        Gson gson = new Gson();

                        Log.d("MyFirebaseInstance", "response: " + gson.toJson(response.body()));

                        DoneOrderData responseData = response.body();

                        if (responseData.getErr_id()Fire == 0) {

                            Log.d("MyFirebaseInstance", "fbtoken saved");

                        } else {
                            //Global.showErrId(context, responseData.err_id, errDialodListener);
                            Log.d("MyFirebaseInstance", "fbtoken not saved");
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<DoneOrderData> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });

    }

}

