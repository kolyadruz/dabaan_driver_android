package com.kolyadruz.dabaandriver.networking.model.responseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActiveOrderData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    @SerializedName("order")
    @Expose
    private OrderModel order;

    public Integer getErr_id() {
        return err_id;
    }

    public void setErr_id(Integer err_id) {
        this.err_id = err_id;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }
}
