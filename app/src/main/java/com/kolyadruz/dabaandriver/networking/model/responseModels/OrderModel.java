package com.kolyadruz.dabaandriver.networking.model.responseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderModel {

    @SerializedName("order_id")
    @Expose
    private Integer order_id;

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("point_a")
    @Expose
    private String point_a;
    @SerializedName("point_b")
    @Expose
    private String point_b;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("prim")
    @Expose
    private String prim;

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("payment_id")
    @Expose
    private Integer payment_id;
    @SerializedName("fromApp")
    @Expose
    private Integer fromApp;

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPoint_a() {
        return point_a;
    }

    public void setPoint_a(String point_a) {
        this.point_a = point_a;
    }

    public String getPoint_b() {
        return point_b;
    }

    public void setPoint_b(String point_b) {
        this.point_b = point_b;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrim() {
        return prim;
    }

    public void setPrim(String prim) {
        this.prim = prim;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(Integer payment_id) {
        this.payment_id = payment_id;
    }

    public Integer getFromApp() {
        return fromApp;
    }

    public void setFromApp(Integer fromApp) {
        this.fromApp = fromApp;
    }
}
