package com.kolyadruz.dabaandriver.networking.model.api;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ActiveOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CancelOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ComingOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CurrentLocationDriverData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.DoneOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.NotificationsData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrdersListData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.TakeOrderData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface ApiInterface {

    @POST("drv_autologin")
    Observable<AutoLoginData> autologin(@Body RequestBody requestBody);

    @POST("drvlogin")
    Observable<LoginData> login(@Body RequestBody requestBody);

    @POST("orderslist")
    Observable<OrdersListData> getorderslist(@Body RequestBody requestBody);

    @POST("takeorder")
    Observable<TakeOrderData> takeorder(@Body RequestBody requestBody);

    @POST("drv_coming_order")
    Observable<ComingOrderData> atorderplace(@Body RequestBody requestBody);

    @POST("drv_done_order")
    Observable<DoneOrderData> doneorder(@Body RequestBody requestBody);

    @POST("drv_cancelorder")
    Observable<CancelOrderData> cancelorder(@Body RequestBody requestBody);

    @POST("drv_activeorder")
    Observable<ActiveOrderData> getactiveorder(@Body RequestBody requestBody);

    @POST("updatetoken")
    Observable<DoneOrderData> updatetoken(@Body RequestBody requestBody);

    @POST("drv_notifications")
    Observable<NotificationsData> notifications(@Body RequestBody requestBody);

    @POST("currentlocationdriver")
    Observable<CurrentLocationDriverData> currentlocationdriver(@Body RequestBody requestBody);
}
