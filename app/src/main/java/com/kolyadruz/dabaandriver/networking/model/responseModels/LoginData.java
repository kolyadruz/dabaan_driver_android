package com.kolyadruz.dabaandriver.networking.model.responseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("notifications")
    @Expose
    private Integer notifications;

    public Integer getErr_id() {
        return err_id;
    }

    public void setErr_id(Integer err_id) {
        this.err_id = err_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getNotifications() {
        return notifications;
    }

    public void setNotifications(Integer notifications) {
        this.notifications = notifications;
    }
}
