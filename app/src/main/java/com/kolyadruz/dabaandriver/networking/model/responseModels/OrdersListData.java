package com.kolyadruz.dabaandriver.networking.model.responseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrdersListData {

    @SerializedName("err_id")
    @Expose
    private Integer err_id;

    @SerializedName("balance")
    @Expose
    private String balance;

    @SerializedName("active")
    @Expose
    private OrderModel active;

    @SerializedName("orders")
    @Expose
    private List<OrderModel> orders;

    public Integer getErr_id() {
        return err_id;
    }

    public void setErr_id(Integer err_id) {
        this.err_id = err_id;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public OrderModel getActive() {
        return active;
    }

    public void setActive(OrderModel active) {
        this.active = active;
    }

    public List<OrderModel> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderModel> orders) {
        this.orders = orders;
    }
}

