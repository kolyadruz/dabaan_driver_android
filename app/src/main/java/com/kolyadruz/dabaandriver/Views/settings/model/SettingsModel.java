package com.kolyadruz.dabaandriver.Views.settings.model;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.NotificationsData;

import rx.Observable;

public interface SettingsModel {

    Observable<NotificationsData> notifications(RequestBody requestBody);

}
