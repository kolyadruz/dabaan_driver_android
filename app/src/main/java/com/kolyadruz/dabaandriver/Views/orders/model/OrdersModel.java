package com.kolyadruz.dabaandriver.Views.orders.model;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ActiveOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CancelOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ComingOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CurrentLocationDriverData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.DoneOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrdersListData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.TakeOrderData;

import rx.Observable;

public interface OrdersModel {

    Observable<CurrentLocationDriverData> currentLocationDriver(RequestBody requestBody);

    Observable<OrdersListData> getOrdersList(RequestBody requestBody);
    Observable<ActiveOrderData> getActiveOrder(RequestBody requestBody);

    Observable<TakeOrderData> takeOrder(RequestBody requestBody);
    Observable<ComingOrderData> atOrderPlace(RequestBody requestBody);
    Observable<DoneOrderData> doneOrder(RequestBody requestBody);
    Observable<CancelOrderData> cancelOrder(RequestBody requestBody);

}
