package com.kolyadruz.dabaandriver.Views.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.gson.Gson;
import com.kolyadruz.dabaandriver.Global;
import com.kolyadruz.dabaandriver.OnErrDialogCLickedListener;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.networking.NetworkService;
import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.NotificationsData;
import com.kolyadruz.dabaandriver.Views.auth.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSettings extends AppCompatActivity implements OnErrDialogCLickedListener {

    AlertDialog.Builder ad;

    String token;

    Integer notifications = 0;

    Activity activity;
    OnErrDialogCLickedListener errDialodListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        setContentView(R.layout.activity_user_settings);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

        activity = this;
        errDialodListener = this;

        token = mSettings.getString("token", "");
    }

    public void saveTapped(View view){
        finish();
    }

    public void changeThemeTapped(View view){
        startDialog();
    }

    public void notificationsTapped(View view) {

        Intent intent = new Intent();
        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());
            startActivity(intent);

        } else {

            intent.putExtra("app_package", getPackageName());
            intent.putExtra("app_uid", getApplicationInfo().uid);

        }

    }

    public void startDialog(){
        final String[] actions ={"Стандартная", "Патриотичная", "Светлая", "Отмена"};
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Выберите тему");

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = mSettings.edit();

        ad.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 3:
                        break;
                    default:
                        editor.putInt("theme", item);
                        editor.commit();

                        finish();
                        startActivity(getIntent());

                        break;
                }
            }
        });



        ad.show();
    }

    public void exitTapped(View view){
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("token", "");
        editor.commit();

        Intent intent = new Intent(getApplicationContext(), Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);
    }


    private void changeMode(String token) {

        RequestBody body = new RequestBody();
        body.token = token;

        NetworkService.getInstance()
                .getJSONApi()
                .notifications(body)
                .enqueue(new Callback<NotificationsData>() {
                    @Override
                    public void onResponse(@NonNull Call<NotificationsData> call, @NonNull Response<NotificationsData> response) {

                        if (!response.isSuccessful()) {
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("Settings.ChangeMode", "response: " + gson.toJson(response.body()));

                        NotificationsData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            notifications = responseData.getNotifications();

                        } else {
                            Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<NotificationsData> call, @NonNull Throwable t) {
                        Global.showErrorConnection(activity, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        changeMode(token);
    }

}
