package com.kolyadruz.dabaandriver.Views.orders.presenter;

import android.util.Log;

import com.kolyadruz.dabaandriver.Views.orders.model.OrdersModel;
import com.kolyadruz.dabaandriver.Views.orders.model.OrdersModelImpl;
import com.kolyadruz.dabaandriver.Views.orders.view.OrdersView;
import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrdersListData;

import rx.Observer;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

public class OrdersPresenterImpl implements OrdersPresenter {

    private OrdersModel model = new OrdersModelImpl();

    private OrdersView view;
    private Subscription subscription = Subscriptions.empty();

    @Override
    public void getOrders() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        String token = view.getToken();

        RequestBody requestBody = new RequestBody();
        requestBody.token = token;

        subscription = model.getOrdersList(requestBody)
                .subscribe(new Observer<OrdersListData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(view.toString(), "completed request");
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d(view.toString(), "error: " + e.getMessage());

                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(OrdersListData ordersListData) {
                        if (ordersListData != null) {

                            if (ordersListData.getErr_id() == 0) {

                                Log.d(view.toString(), "data not null");
                                view.showOrdersListData(ordersListData);

                            } else {

                                Log.d(view.toString(), "data with errid");
                                view.showError("err_id:" + ordersListData.getErr_id());

                            }

                        } else {
                            Log.d(view.toString(), "data null");
                            view.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void getActiveOrder() {

    }

    @Override
    public void takeOrder() {

    }

    @Override
    public void atOrderPlace() {

    }

    @Override
    public void cancelOrder() {

    }

    @Override
    public void onStop() {

    }
}
