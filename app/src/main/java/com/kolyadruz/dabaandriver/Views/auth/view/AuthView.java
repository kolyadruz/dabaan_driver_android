package com.kolyadruz.dabaandriver.Views.auth.view;

import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;

public interface AuthView {

    void showAutoLoginData(AutoLoginData autoLoginData);

    void showLoginData(LoginData loginData);

    void showError(String error);

    void showEmptyList();

    String getToken();
    String getFbToken();

    String getPhone();
    String getPass();

}
