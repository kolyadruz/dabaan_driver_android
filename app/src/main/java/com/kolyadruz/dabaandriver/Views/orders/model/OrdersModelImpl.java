package com.kolyadruz.dabaandriver.Views.orders.model;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.api.ApiInterface;
import com.kolyadruz.dabaandriver.networking.model.api.ApiModule;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ActiveOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CancelOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ComingOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CurrentLocationDriverData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.DoneOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrdersListData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.TakeOrderData;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OrdersModelImpl implements OrdersModel {

    ApiInterface apiInterface = ApiModule.getApiInterface();

    @Override
    public Observable<CurrentLocationDriverData> currentLocationDriver(RequestBody requestBody) {
        return apiInterface.currentlocationdriver(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<OrdersListData> getOrdersList(RequestBody requestBody) {
        return apiInterface.getorderslist(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ActiveOrderData> getActiveOrder(RequestBody requestBody) {
        return apiInterface.getactiveorder(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<TakeOrderData> takeOrder(RequestBody requestBody) {
        return apiInterface.takeorder(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ComingOrderData> atOrderPlace(RequestBody requestBody) {
        return apiInterface.atorderplace(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<DoneOrderData> doneOrder(RequestBody requestBody) {
        return apiInterface.doneorder(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<CancelOrderData> cancelOrder(RequestBody requestBody) {
        return apiInterface.cancelorder(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
