package com.kolyadruz.dabaandriver.Views.orders;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.kolyadruz.dabaandriver.App;
import com.kolyadruz.dabaandriver.Global;
import com.kolyadruz.dabaandriver.OnErrDialogCLickedListener;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.Views.orders.adapters.OrdersAdapter;
import com.kolyadruz.dabaandriver.networking.NetworkService;
import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ActiveOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.CancelOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.ComingOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.DoneOrderData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrderModel;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrdersListData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.TakeOrderData;
import com.kolyadruz.dabaandriver.Views.settings.UserSettings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersList extends AppCompatActivity implements AdapterView.OnItemClickListener, OnErrDialogCLickedListener, LocationListener {

    AlertDialog.Builder ad;

    String token;
    String balance;
    OrdersAdapter ordersAdapter;
    ListView ordersList;
    List<OrderModel> ordersArray = new ArrayList<>();

    Timer taskTimer = new Timer();
    Timer taskTimerActive = new Timer();
    runGetOrders mRunGetOrders;
    runGetActiveOrder mRunGetActiveOrder;

    TextView noOrders;
    TextView balanceTxt;

    Integer selectedOrderID = 0;
    String selectedOrderAddress ="";
    String selectedOrderPhone ="";

    ConstraintLayout whiteTint;
    ConstraintLayout takenOrderDialog;

    ConstraintLayout rootConstraint;
    ConstraintSet noActiveOrder;
    ConstraintSet withActiveOrder;
    Boolean isDialogShown = false;

    String takenOrderClientPhone;
    Integer takenOrderStatus;
    Integer takenOrderID;

    TextView pointAtxt;
    TextView pointBtxt;
    TextView noteTxt;
    TextView costTxt;

    TextView orderStatusTxt;
    TextView cancelBtnTextView;

    Boolean isCanceled;

    Activity activity;
    OnErrDialogCLickedListener errDialodListener;

    Integer currentTheme;

    private Socket mSocket;

    //GPS
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 5000;

    LocationManager locationManager;
    Location loc;

    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            setContentView(R.layout.activity_orderslist);

            rootConstraint = findViewById(R.id.root);

            noActiveOrder = new ConstraintSet();
            noActiveOrder.clone(rootConstraint);

            withActiveOrder = new ConstraintSet();
            withActiveOrder.clone(this, R.layout.activity_orderslist_taken);

        } else {

            setContentView(R.layout.activity_orderslist_taken);


        }

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

        currentTheme = theme;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        token = mSettings.getString("token", "");

        App app = (App) getApplication();
        mSocket = app.getSocket();
        mSocket.connect();

        //checkGpsParams();

        activity = OrdersList.this;
        errDialodListener = this;

        whiteTint = findViewById(R.id.whiteTint);
        whiteTint.setVisibility(View.GONE);
        takenOrderDialog = findViewById(R.id.active_order_dialog);
        takenOrderDialog.setVisibility(View.GONE);

        ordersList = findViewById(R.id.ordersList);
        noOrders = findViewById(R.id.noOrdersTxt);
        balanceTxt = findViewById(R.id.balanceTxt);

        ordersAdapter = new OrdersAdapter(this, ordersArray, R.layout.cell_order);

        ordersList.setOnItemClickListener(this);
        ordersList.setAdapter(ordersAdapter);

        pointAtxt = findViewById(R.id.pointAtxt);
        pointBtxt = findViewById(R.id.pointBtxt);
        noteTxt = findViewById(R.id.noteTxt);
        costTxt = findViewById(R.id.costTxt);

        orderStatusTxt = findViewById(R.id.orderStatusTxt);
        cancelBtnTextView = findViewById(R.id.cancelBtnTextView);

        try {
            takenOrderStatus = getIntent().getExtras().getInt("status");
            takenOrderID = getIntent().getExtras().getInt("order_id");
            takenOrderClientPhone = getIntent().getExtras().getString("phone");
            showTakenDialog();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        Integer theme = mSettings.getInt("theme", 0);

        if (theme != currentTheme) {
            currentTheme = theme;
            finish();
            startActivity(getIntent());
        }

        getOrders(token);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (taskTimer != null) {
            taskTimer.cancel();
        }
    }

    @Override
    public void onDestroy() {
        mSocket.disconnect();
        if (taskTimer != null) {
            taskTimer.cancel();
        }
        super.onDestroy();
    }

    public void runGetOrdersWithInterval() {
        if (taskTimer != null) {
            taskTimer.cancel();
        }

        taskTimer = new Timer();
        mRunGetOrders = new runGetOrders();
        taskTimer.schedule(mRunGetOrders, 1000);
    }

    public void showBonusInfo(View view) {
        view.setEnabled(false);
        Intent intent = new Intent(getApplicationContext(), BalanceInfo.class);
        startActivity(intent);
        view.setEnabled(true);
    }

    public void settingsTapped(View view){
        view.setEnabled(false);
        Intent intent = new Intent(getApplicationContext(), UserSettings.class);
        startActivity(intent);
        view.setEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (taskTimer != null) {
            taskTimer.cancel();
        }

        OrderModel selectedOrder = ordersArray.get(position);
        selectedOrderID = selectedOrder.getOrder_id();
        selectedOrderAddress = selectedOrder.getPoint_a();
        selectedOrderPhone = selectedOrder.getPoint_b();

        Log.d("OrdersList:","selectedOrderID="+selectedOrderID);

        startActionDialog(selectedOrderAddress, selectedOrderID);

        taskTimer = new Timer();
        mRunGetOrders = new runGetOrders();
        taskTimer.schedule(mRunGetOrders, 1000);
    }

    public void startActionDialog(final String title, final Integer selectedOrderID){
        final String[] actions ={"Принять", "Закрыть окно"};
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Действие с заказом: "+title);

        ad.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        startArrivalTimeDialog(title, selectedOrderID);
                        break;
                    case 1:
                        break;
                    default:
                        break;
                }
            }
        });

        ad.show();
    }

    public void startArrivalTimeDialog(final String title, final Integer selectedOrderID) {

        final Integer[] arrivalTimeVariants = {5, 10, 20, 30, 40};

        final String[] actions = new String[arrivalTimeVariants.length + 1]; //{"5 минут", "10 минут", "20 минут", "30 минут", "40 минут", "Отмена"};

        for (int i = 0; i < arrivalTimeVariants.length; i++) {
            actions[i] = arrivalTimeVariants[i] + " минут";
        }
        actions[arrivalTimeVariants.length] = "Отмена";

        ad = new AlertDialog.Builder(this);
        ad.setTitle("Прибуду на" + title + "через: ");

        ad.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item < arrivalTimeVariants.length) {
                    takeOrder(token, selectedOrderID, arrivalTimeVariants[item]);
                }

            }
        });

        ad.show();
    }

    class runGetOrders extends TimerTask {
        @Override
        public void run() {
            if(this == null)
                return;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getOrders(token);
                }
            });
        }
    }

    private void getOrders(String token) {

        RequestBody body = new RequestBody();
        body.token = token;

        NetworkService.getInstance()
                .getJSONApi()
                .orderslist(body)
                .enqueue(new Callback<OrdersListData>() {
                    @Override
                    public void onResponse(@NonNull Call<OrdersListData> call, @NonNull Response<OrdersListData> response) {

                        if (!response.isSuccessful()) {
                            runGetOrdersWithInterval();
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("OrdersList.GetOrders", "response: " + gson.toJson(response.body()));

                        OrdersListData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            balance = responseData.getBalance();
                            if (balance.equals("null")) {
                                balanceTxt.setText("");
                            } else {
                                balanceTxt.setText("" + balance);
                            }

                            //Log.d("OrdersList:", "Список успешно получен");

                            ordersArray.clear();

                            if (responseData.getOrders().size() > 0) {
                                noOrders.setVisibility(View.INVISIBLE);
                            } else {
                                noOrders.setVisibility(View.VISIBLE);
                            }

                            //ordersAdapter.notifyDataSetChanged();

                            ordersArray = responseData.getOrders();
                            ordersAdapter.UpdateData(ordersArray);

                            runGetOrdersWithInterval();

                            if (responseData.getActive().getOrder_id() != 0) {
                                if (!isDialogShown) {
                                    showTakenDialog();
                                }
                            }

                        } else {
                            Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                            runGetOrdersWithInterval();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<OrdersListData> call, @NonNull Throwable t) {
                        //Global.showErrorConnection(activity, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }

    private void takeOrder(String token, Integer order_id, Integer arrivalTime) {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;
        body.arrivalTime = arrivalTime;

        NetworkService.getInstance()
                .getJSONApi()
                .takeorder(body)
                .enqueue(new Callback<TakeOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<TakeOrderData> call, @NonNull Response<TakeOrderData> response) {

                        if (!response.isSuccessful()) {
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("OrdersList.TakeOrder", "response: " + gson.toJson(response.body()));

                        TakeOrderData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            takenOrderID = selectedOrderID;
                            takenOrderStatus = 2;
                            takenOrderClientPhone = selectedOrderPhone;

                            isCanceled = false;
                            orderStatusTxt.setText("ВЫ НА ЗАКАЗЕ");
                            cancelBtnTextView.setText("Отменить");
                            showTakenDialog();

                        } else {
                            //Global.showErrId(context, responseData.err_id, errDialodListener);
                            //runGetOrdersWithInterval();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<TakeOrderData> call, @NonNull Throwable t) {
                        //Global.showErrorConnection(activity, errDialodListener);
                        //runGetOrdersWithInterval();
                        t.printStackTrace();
                    }
                });

    }


    private void showTakenDialog() {

        runDrvActiveOrderWithInterval();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            TransitionManager.beginDelayedTransition(rootConstraint);
            withActiveOrder.applyTo(rootConstraint);

        } else {

            whiteTint.setVisibility(View.VISIBLE);
            takenOrderDialog.setVisibility(View.VISIBLE);

        }

        ordersList.setEnabled(false);

        isDialogShown = true;
    }

    private void hideTakenDialog() {

        cancelDrvActiveOrderWithInterval();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            TransitionManager.beginDelayedTransition(rootConstraint);
            noActiveOrder.applyTo(rootConstraint);

        } else {

            whiteTint.setVisibility(View.GONE);
            takenOrderDialog.setVisibility(View.GONE);

        }

        ordersList.setEnabled(true);

        isDialogShown = false;
    }


    public void callClient(View v) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel: +" + takenOrderClientPhone));
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
        }
        else
        {
            this.startActivity(intent);
        }
    }

    public void atPlace(View v) {
        atPlace(token, takenOrderID);
    }

    public void completedOrder(View v) {
        doneOrder(token, takenOrderID);
    }

    public void cancelOrder(View v) {
        cancelOrder(token, takenOrderID, 3);
    }

    private void atPlace(String token, Integer order_id) {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;

        NetworkService.getInstance()
                .getJSONApi()
                .comingorder(body)
                .enqueue(new Callback<ComingOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<ComingOrderData> call, @NonNull Response<ComingOrderData> response) {

                        if (!response.isSuccessful()) {
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("OrdersList.AtPlace", "response: " + gson.toJson(response.body()));

                        ComingOrderData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            Log.d("OrdersList.AtPlace:", "Статус успешно изменен");

                        } else {
                            Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ComingOrderData> call, @NonNull Throwable t) {
                        //Global.showErrorConnection(activity, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }

    private void doneOrder(String token, Integer order_id) {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;

        NetworkService.getInstance()
                .getJSONApi()
                .doneorder(body)
                .enqueue(new Callback<DoneOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<DoneOrderData> call, @NonNull Response<DoneOrderData> response) {

                        if (!response.isSuccessful()) {
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("OrdersList.DoneOrder", "response: " + gson.toJson(response.body()));

                        DoneOrderData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            Log.d("OrdersList.DoneOrder:", "Статус успешно изменен");
                            hideTakenDialog();

                        } else {
                            Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<DoneOrderData> call, @NonNull Throwable t) {
                        //Global.showErrorConnection(activity, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }

    private void cancelOrder(String token, Integer order_id, Integer reason_id) {

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;
        body.reason_id = reason_id;

        NetworkService.getInstance()
                .getJSONApi()
                .cancelorder(body)
                .enqueue(new Callback<CancelOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<CancelOrderData> call, @NonNull Response<CancelOrderData> response) {

                        if (!response.isSuccessful()) {
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("OrdersList.CancelOrder", "response: " + gson.toJson(response.body()));

                        CancelOrderData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            Log.d("OrdersList.CancelOrder:", "Статус успешно изменен");
                            hideTakenDialog();

                        } else {
                            Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CancelOrderData> call, @NonNull Throwable t) {
                        //Global.showErrorConnection(activity, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }

    public void runDrvActiveOrderWithInterval() {
        if (taskTimerActive != null) {
            taskTimerActive.cancel();
        }

        taskTimerActive = new Timer();
        mRunGetActiveOrder = new runGetActiveOrder();
        taskTimerActive.schedule(mRunGetActiveOrder, 1000);
    }

    public void cancelDrvActiveOrderWithInterval() {
        if (taskTimerActive != null) {
            taskTimerActive.cancel();
        }
    }

    class runGetActiveOrder extends TimerTask {
        @Override
        public void run() {
            if(this == null)
                return;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getActiveOrder(token);
                }
            });
        }
    }

    private void getActiveOrder(String token) {

        RequestBody body = new RequestBody();
        body.token = token;

        NetworkService.getInstance()
                .getJSONApi()
                .activeorder(body)
                .enqueue(new Callback<ActiveOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<ActiveOrderData> call, @NonNull Response<ActiveOrderData> response) {

                        if (!response.isSuccessful()) {
                            runDrvActiveOrderWithInterval();
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("OrdersList.ActiveOrder", "response: " + gson.toJson(response.body()));

                        ActiveOrderData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            Log.d("OrdersList.ActiveOrder:","Список успешно получен");

                            takenOrderID = responseData.getOrder().getOrder_id();

                            if (takenOrderID == 0) {
                                hideTakenDialog();
                            } else {

                                if (responseData.getOrder().getPhone() != null) {

                                    takenOrderClientPhone = responseData.getOrder().getPhone();
                                    pointAtxt.setText(responseData.getOrder().getPoint_a());
                                    pointBtxt.setText(responseData.getOrder().getPoint_b());
                                    costTxt.setText(responseData.getOrder().getPrice());
                                    noteTxt.setText(responseData.getOrder().getPrim());
                                    takenOrderStatus = responseData.getOrder().getStatus();

                                    switch (takenOrderStatus) {
                                        case 4:
                                            isCanceled = true;
                                            orderStatusTxt.setText("ЗАКАЗ ОТМЕНЕН КЛИЕНТОМ");
                                            cancelBtnTextView.setText("Закрыть");
                                            break;
                                        case 8:
                                            isCanceled = true;
                                            orderStatusTxt.setText("ЗАКАЗ ОТМЕНЕН ДИСПЕТЧЕРОМ");
                                            cancelBtnTextView.setText("Закрыть");
                                            break;
                                        case 9:
                                            orderStatusTxt.setText("ВЫ НА МЕСТЕ");
                                            break;
                                        default:
                                            break;
                                    }

                                    runDrvActiveOrderWithInterval();
                                }
                            }

                        } else {
                            runDrvActiveOrderWithInterval();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ActiveOrderData> call, @NonNull Throwable t) {
                        //Global.showErrorConnection(activity, errDialodListener);
                        runDrvActiveOrderWithInterval();
                        t.printStackTrace();
                    }
                });

    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {

    }


    //LocationService
    private void checkGpsParams() {

        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            getLocation();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            //updateUI(location);
            sendLocation(location);
        }
    }

    private void sendLocation(Location location) {

        JSONObject data = new JSONObject();

        try {

            data.put("token", token);
            data.put("secret", "6sabvg10-9cd6-4843-8489-9704b3jxda2b");
            data.put("lat", location.getLatitude());
            data.put("lon", location.getLongitude());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.d(TAG, "Sending Location: " + data.toString()       );


        mSocket.emit("sendDriverGeo", data);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null) {
                            sendLocation(loc);
                        }
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null) {
                            sendLocation(loc);
                        }
                    }
                } else {
                    loc.setLatitude(0);
                    loc.setLongitude(0);
                    //sendLocation(loc);
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("Разрешение требуется дл работы с местоположением и картами. Пожалуйста предоставьте доступ.",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(permissionsRejected.toArray(
                                                new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                    }
                                }
                            });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS отключен!");
        alertDialog.setMessage("Желаете включить GPS?");
        alertDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(OrdersList.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Отмена", null)
                .create()
                .show();
    }
}