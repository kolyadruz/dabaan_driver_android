package com.kolyadruz.dabaandriver.Views.settings.model;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.api.ApiInterface;
import com.kolyadruz.dabaandriver.networking.model.api.ApiModule;
import com.kolyadruz.dabaandriver.networking.model.responseModels.NotificationsData;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SettingsModelImpl implements SettingsModel {

    ApiInterface apiInterface = ApiModule.getApiInterface();

    @Override
    public Observable<NotificationsData> notifications(RequestBody requestBody) {
        return apiInterface.notifications(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
