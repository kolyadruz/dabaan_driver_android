package com.kolyadruz.dabaandriver.Views.auth.presenter;

public interface AuthPresenter {

    void autoLogin();

    void login();

    void onStop();

}
