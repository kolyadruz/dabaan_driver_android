package com.kolyadruz.dabaandriver.Views.orders.view;

import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrdersListData;

public interface OrdersView {

    void showError(String error);
    void showEmptyList();

    void showOrdersListData(OrdersListData ordersListData);

    String getToken();
    Integer getOrderId();



}
