package com.kolyadruz.dabaandriver.Views.orders.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.networking.model.responseModels.OrderModel;

import java.util.List;

/**
 * Created by kolyadruz on 04.12.2017.
 */

public class OrdersAdapter extends BaseAdapter {

    private Activity activity;

    private List<OrderModel> orders;

    private int layoutID;
    private LayoutInflater inflater=null;

    //public order_id;

    public OrdersAdapter(Activity activity, List<OrderModel> orders, int layoutID) {
        this.activity = activity;
        this.orders = orders;
        this.layoutID = layoutID;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return orders.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        OrdersListCell ordersListCell;
        if(editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            ordersListCell = new OrdersListCell(cellView);
            cellView.setTag(ordersListCell);
        } else {
            ordersListCell = (OrdersListCell) cellView.getTag();
        }

        final OrderModel order = orders.get(position);

        ordersListCell.pointAtxt.setText(order.getPoint_a());
        ordersListCell.pointBtxt.setText(order.getPoint_b());
        ordersListCell.costTxt.setText(order.getPrice());
        ordersListCell.noteTxt.setText(order.getPrim());

        if(order.getPayment_id() == 0) {
            ordersListCell.bgcolor.setBackgroundColor(Color.WHITE);
        } else {
            ordersListCell.bgcolor.setBackgroundColor(Color.YELLOW);
        }

        if (order.getFromApp() == 0) {
            ordersListCell.phoneLogo.setVisibility(View.INVISIBLE);
        } else {
            ordersListCell.phoneLogo.setVisibility(View.VISIBLE);
        }

        Log.d("OrdersAdapter", "Added CELL");

        return cellView;
    }

    class OrdersListCell {
        public TextView pointAtxt;
        public TextView pointBtxt;
        public TextView costTxt;
        public TextView noteTxt;
        public ConstraintLayout bgcolor;

        public ImageView phoneLogo;

        public OrdersListCell(View base) {
            pointAtxt = (TextView) base.findViewById(R.id.pointAtxt);
            pointBtxt = (TextView) base.findViewById(R.id.pointBtxt);
            costTxt = (TextView) base.findViewById(R.id.costTxt);
            noteTxt = (TextView) base.findViewById(R.id.primTxt);
            bgcolor = base.findViewById(R.id.bgcolor);
            phoneLogo = base.findViewById(R.id.phoneLogo);
        }
    }

    public void UpdateData(List<OrderModel> orders) {

        this.orders = orders;
        notifyDataSetChanged();

    }

}