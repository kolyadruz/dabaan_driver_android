package com.kolyadruz.dabaandriver.Views.auth.presenter;

import android.util.Log;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.Views.auth.model.AuthModel;
import com.kolyadruz.dabaandriver.Views.auth.model.AuthModelImpl;
import com.kolyadruz.dabaandriver.Views.auth.view.AuthView;
import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;

import rx.Observer;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

public class AuthPresenterImpl implements AuthPresenter {

    private AuthModel model = new AuthModelImpl();

    private AuthView view;
    private Subscription subscription = Subscriptions.empty();

    public AuthPresenterImpl(AuthView view)
    {
        this.view = view;
    }

    @Override
    public void autoLogin() {

        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        String token = view.getToken();
        String fb_token = view.getFbToken();

        RequestBody requestBody = new RequestBody();
        requestBody.token = token;
        requestBody.fb_token = fb_token;

        subscription = model.autoLogin(requestBody)
                .subscribe(new Observer<AutoLoginData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(view.toString(), "completed request");
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d(view.toString(), "error: " + e.getMessage());

                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(AutoLoginData autoLoginData) {
                        if (autoLoginData != null) {

                            if (autoLoginData.getErr_id() == 0) {

                                Log.d(view.toString(), "data not null");
                                view.showAutoLoginData(autoLoginData);

                            } else {

                                Log.d(view.toString(), "data with errid");
                                view.showError("err_id:" + autoLoginData.getErr_id());

                            }

                        } else {
                            Log.d(view.toString(), "data null");
                            view.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void login() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        String phone = view.getPhone();
        String pass = view.getPass();

        RequestBody requestBody = new RequestBody();
        requestBody.phone = phone;
        requestBody.pass = pass;

        subscription = model.login(requestBody)
                .subscribe(new Observer<LoginData>() {
                    @Override
                    public void onCompleted() {
                        Log.d(view.toString(), "completed request");
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d(view.toString(), "error: " + e.getMessage());

                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(LoginData loginData) {
                        if (loginData != null) {

                            if (loginData.getErr_id() == 0) {

                                Log.d(view.toString(), "data not null");
                                view.showLoginData(loginData);

                            } else {

                                Log.d(view.toString(), "data with errid");
                                view.showError("err_id:" + loginData.getErr_id());

                            }

                        } else {
                            Log.d(view.toString(), "data null");
                            view.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void onStop() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
