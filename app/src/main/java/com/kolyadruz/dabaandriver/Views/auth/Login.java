package com.kolyadruz.dabaandriver.Views.auth;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.kolyadruz.dabaandriver.Global;
import com.kolyadruz.dabaandriver.OnErrDialogCLickedListener;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.Views.auth.presenter.AuthPresenter;
import com.kolyadruz.dabaandriver.Views.auth.presenter.AuthPresenterImpl;
import com.kolyadruz.dabaandriver.Views.auth.view.AuthView;
import com.kolyadruz.dabaandriver.networking.NetworkService;
import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;
import com.kolyadruz.dabaandriver.Views.orders.OrdersList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements OnErrDialogCLickedListener, AuthView {

    String fbtoken = "";

    EditText phoneField;
    EditText passField;

    Button enterButton;

    Activity activity;
    OnErrDialogCLickedListener errDialodListener;

    AuthPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = this;
        errDialodListener = this;

        phoneField = findViewById(R.id.phoneField);
        passField = findViewById(R.id.passField);

        enterButton = findViewById(R.id.enterButton);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        fbtoken = mSettings.getString("fbtoken", "");

        presenter = new AuthPresenterImpl(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    public void enter(View view){
        view.setEnabled(false);

        if (phoneField.length() > 9) {
            runDrvLogin();
        } else {
            Global.showErrId(activity, 1, errDialodListener);
        }

        view.setEnabled(true);
    }

    private void runDrvLogin() {

        presenter.login();
        //login(phone, pass, fbtoken);

    }

    /*private void login(String phone, String pass, String fbtoken) {

        RequestBody body = new RequestBody();
        body.fb_token = fbtoken;
        body.phone = phone;
        body.pass = pass;

        NetworkService.getInstance()
                .getJSONApi()
                .login(body)
                .enqueue(new Callback<LoginData>() {
                    @Override
                    public void onResponse(@NonNull Call<LoginData> call, @NonNull Response<LoginData> response) {

                        if (!response.isSuccessful()) {
                            return;
                        }

                        Gson gson = new Gson();

                        Log.d("Login", "response: " + gson.toJson(response.body()));

                        LoginData responseData = response.body();

                        if (responseData.getErr_id() == 0) {

                            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString("token", responseData.getToken());
                            editor.putInt("notifications", responseData.getNotifications());
                            editor.commit();

                            Log.d("Login:","Переход в OrdersList.java");

                            Intent intent = new Intent(getApplicationContext(), OrdersList.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);

                        } else {
                            Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<LoginData> call, @NonNull Throwable t) {
                        Global.showErrorConnection(activity, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }*/

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        runDrvLogin();
    }

    @Override
    public void showAutoLoginData(AutoLoginData autoLoginData) {

    }

    @Override
    public void showLoginData(LoginData loginData) {
        //LoginData responseData = response.body();

        if (loginData.getErr_id() == 0) {

            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString("token", loginData.getToken());
            editor.putInt("notifications", loginData.getNotifications());
            editor.commit();

            Log.d("Login:","Переход в OrdersList.java");

            Intent intent = new Intent(getApplicationContext(), OrdersList.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            startActivity(intent);

        } else {
            Global.showErrId(activity, loginData.getErr_id(), errDialodListener);
        }
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showEmptyList() {

    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public String getFbToken() {
        return null;
    }

    @Override
    public String getPhone() {
        return phoneField.getText().toString();
    }

    @Override
    public String getPass() {
        return passField.getText().toString();
    }
}
