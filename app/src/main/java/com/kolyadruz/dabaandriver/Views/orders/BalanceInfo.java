package com.kolyadruz.dabaandriver.Views.orders;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.kolyadruz.dabaandriver.R;

public class BalanceInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        setContentView(R.layout.activity_balance_info);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }
    }
}
