package com.kolyadruz.dabaandriver.Views.orders.presenter;

public interface OrdersPresenter {

    void getOrders();

    void getActiveOrder();

    void takeOrder();

    void atOrderPlace();

    void cancelOrder();

    void onStop();

}
