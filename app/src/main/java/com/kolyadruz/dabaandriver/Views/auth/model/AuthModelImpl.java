package com.kolyadruz.dabaandriver.Views.auth.model;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.api.ApiInterface;
import com.kolyadruz.dabaandriver.networking.model.api.ApiModule;
import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthModelImpl implements AuthModel {

    ApiInterface apiInterface = ApiModule.getApiInterface();

    @Override
    public Observable<AutoLoginData> autoLogin(RequestBody requestBody) {
        return apiInterface.autologin(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<LoginData> login(RequestBody requestBody) {
        return apiInterface.login(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}