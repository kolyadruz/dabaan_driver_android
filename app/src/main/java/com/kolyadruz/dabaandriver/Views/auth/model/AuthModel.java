package com.kolyadruz.dabaandriver.Views.auth.model;

import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;

import rx.Observable;

public interface AuthModel {

    Observable<AutoLoginData> autoLogin(RequestBody requestBody);
    Observable<LoginData> login(RequestBody requestBody);

}
