package com.kolyadruz.dabaandriver.Views.auth;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.kolyadruz.dabaandriver.Global;
import com.kolyadruz.dabaandriver.OnErrDialogCLickedListener;
import com.kolyadruz.dabaandriver.R;
import com.kolyadruz.dabaandriver.Views.auth.presenter.AuthPresenter;
import com.kolyadruz.dabaandriver.Views.auth.presenter.AuthPresenterImpl;
import com.kolyadruz.dabaandriver.Views.auth.view.AuthView;
import com.kolyadruz.dabaandriver.networking.NetworkService;
import com.kolyadruz.dabaandriver.networking.RequestBody;
import com.kolyadruz.dabaandriver.networking.model.responseModels.AutoLoginData;
import com.kolyadruz.dabaandriver.Views.orders.OrdersList;
import com.kolyadruz.dabaandriver.networking.model.responseModels.LoginData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutoLogin extends AppCompatActivity implements OnErrDialogCLickedListener, AuthView {

    private static final String TAG = "FireBaseToken";

    String token;
    String fb_token = "";

    Integer notifications;

    Activity activity;
    OnErrDialogCLickedListener errDialodListener;

    AuthPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autologin);

        //FirebaseMessaging.getInstance().subscribeToTopic("drivers");

        activity = this;
        errDialodListener = this;

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        presenter = new AuthPresenterImpl(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Get token
        String tkn = FirebaseInstanceId.getInstance().getToken();

        // Log and toast
        String msg = getString(R.string.msg_token_fmt, tkn);
        fb_token = tkn;

        Log.d(TAG, msg);
        //Toast.makeText(AutoLogin.this, msg, Toast.LENGTH_SHORT).show();

        if (token.equals("")) {
            Log.d("AutoLogin:", "Токен не найден");
            Log.d("AutoLogin:", "Переход в Login.java");

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString("fbtoken", fb_token);
            editor.commit();

            startActivity(intent);

        } else {
            Log.d("AutoLogin:", "Токен найден "+token);
            Log.d("AutoLogin:", "Запуск проверки токена");

            //autologin(token, fb_token);
            presenter.autoLogin();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    /*private void autologin(String token, String fbtoken) {

        RequestBody body = new RequestBody();
        body.fb_token = fbtoken;
        body.token = token;

            NetworkService.getInstance()
                    .getJSONApi()
                    .autologin(body)
                    .enqueue(new Callback<AutoLoginData>() {
            @Override
            public void onResponse(@NonNull Call<AutoLoginData> call, @NonNull Response<AutoLoginData> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                Gson gson = new Gson();

                Log.d("AutoLogin", "response: " + gson.toJson(response.body()));

                AutoLoginData responseData = response.body();

                if (responseData.getErr_id() == 0) {

                    Log.d("AutoLogin:","Токен рабочий");

                    Integer order_id = responseData.getOrder().getOrder_id();

                    notifications = responseData.getNotifications();
                    SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = mSettings.edit();
                    editor.putInt("notifications", notifications);
                    editor.commit();

                    if (order_id == 0) {
                        Log.d("AutoLogin:","Нет активных заказов");
                        Log.d("AutoLogin:","Переход в OrdersList.java");
                        Intent intent = new Intent(getApplicationContext(), OrdersList.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        startActivity(intent);
                    } else {
                        Log.d("AutoLogin:","Есть активный заказ");
                        Log.d("AutoLogin:","Переход в TakenOrder.java");

                        //Intent intent = new Intent(getApplicationContext(), TakenOrder.class);
                        Intent intent = new Intent(getApplicationContext(), OrdersList.class);

                        intent.putExtra("status", responseData.getOrder().getStatus());
                        intent.putExtra("order_id", order_id);
                        intent.putExtra("phone", responseData.getOrder().getPhone());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        startActivity(intent);
                    }

                } else {
                    Global.showErrId(activity, responseData.getErr_id(), errDialodListener);
                }
            }

            @Override
            public void onFailure(@NonNull Call<AutoLoginData> call, @NonNull Throwable t) {
                Global.showErrorConnection(activity, errDialodListener);
                t.printStackTrace();
            }
        });

    }*/

    @Override
    public void onOkButtonCLicked() {
        Log.d("AutoLogin:","Ошибка работы токена. Переход в Login.java");
        Intent intent = new Intent(getApplicationContext(), Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);
    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        //autologin(token, fb_token);
        presenter.autoLogin();
    }

    @Override
    public void showAutoLoginData(AutoLoginData autoLoginData) {
        //AutoLoginData responseData = response.body();

        if (autoLoginData.getErr_id() == 0) {

            Log.d("AutoLogin:","Токен рабочий");

            Integer order_id = autoLoginData.getOrder().getOrder_id();

            notifications = autoLoginData.getNotifications();
            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt("notifications", notifications);
            editor.commit();

            if (order_id == 0) {
                Log.d("AutoLogin:","Нет активных заказов");
                Log.d("AutoLogin:","Переход в OrdersList.java");
                Intent intent = new Intent(getApplicationContext(), OrdersList.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                startActivity(intent);
            } else {
                Log.d("AutoLogin:","Есть активный заказ");
                Log.d("AutoLogin:","Переход в TakenOrder.java");

                //Intent intent = new Intent(getApplicationContext(), TakenOrder.class);
                Intent intent = new Intent(getApplicationContext(), OrdersList.class);

                intent.putExtra("status", autoLoginData.getOrder().getStatus());
                intent.putExtra("order_id", order_id);
                intent.putExtra("phone", autoLoginData.getOrder().getPhone());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                startActivity(intent);
            }

        } else {
            Global.showErrId(activity, autoLoginData.getErr_id(), errDialodListener);
        }
    }

    @Override
    public void showLoginData(LoginData loginData) {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showEmptyList() {

    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public String getFbToken() {
        return fb_token;
    }

    @Override
    public String getPhone() {
        return null;
    }

    @Override
    public String getPass() {
        return null;
    }
}
